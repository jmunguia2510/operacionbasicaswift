class Operacion {
    var simbolo: String = ""
    
    
    func validarNumero(texto:String) -> Double{
        if texto.isEmpty{
            return 0
        }
        
        guard let numero = Double(texto) else{
            return 0
        }
        
        return numero
    }
    
    func validarSimbolo(texto:String) -> Int{
        if texto.isEmpty{
            return 0
        }
        
        if texto.count > 1{
            return 0
        }
        
        if texto == "+" {
            return 1
        } else if texto == "-" {
            return 2
        } else if texto == "*" {
            return 3
        } else if texto == "/" {
            return 4
        } else {
            return 0
        }
        
    }
    
    
    func operar(simbolo:String, numeroA:String, numeroB:String) -> Double{
        let operacion = validarSimbolo(texto: simbolo)
        let numero1 = validarNumero(texto: numeroA)
        let numero2 = validarNumero(texto: numeroB)
        
        if operacion == 1 {
            return numero1 + numero2
        }else if operacion == 2 {
            return numero1 - numero2
        }else if operacion == 3 {
            return numero1 * numero2
        }else if operacion == 4 {
            if numero2 == 0{
                print("No se puede realizar la operacion por ser división entre cero")
                return -1
            }
            return numero1 / numero2
        }else{
            print("No se reconoce el tipo de operador")
            return -1
        }
        
    }
}

let operacion = Operacion()
let resultado = operacion.operar(simbolo: "/", numeroA: "4", numeroB: "0")

if resultado > -1 {
    print("El resultado es: \(resultado)")

}

