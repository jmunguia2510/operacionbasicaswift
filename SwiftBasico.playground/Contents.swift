import UIKit

var str = "Hello, playground"

//MARK: - Constantes --> Constante: SU VALOR NO CAMBIA!
let edadPersona = 32 //Definicion explicita
let nombrePersona: String = "Jhonatan" //Definicion implicita

//MARK: - Variable --> PUEDO CAMBIAR DE VALOR!!
var edadAlumno = 25
var edadProfesor: Int = 31

edadAlumno += 2
edadProfesor  += 5

let nuevaEdadProfesor = Double(edadProfesor) + 0.5

//NIL = es NULO
//MARK: OPTIONAL ? --> Este valor puede ser NULO(NIL)
//MARK: !--> UNWRAP retorna el valor verdadero de un optional
let edadTexto = "32"
var edadEntero: Int? = Int(edadTexto)
//edadEntero! += 4
if edadEntero != nil{
    print("Mi edad es: \(edadEntero!)")

}

//MARK: -Funciones
//
//func nombreFuncion(ALIAS1 variable1:Tipo1, ALIAS2 variable2:Tipo2) --> TipoRetorno
//cuando una funcion no tiene tipo de retorno esto es un tipo void

func imprimir(unTexto texto:String){
    print(texto)
}

imprimir(unTexto: "Hola chicos bienvenidos a Swift")

func dividir(dividendo:Int, divisor:Int) -> Int{ //EL ALIAS ES LA VARIABLE
    return dividendo/divisor
}

let cociente = dividir(dividendo: 12, divisor: 4)


//El _ significa que no tendremos alias no es recomendable
func suma(_ A: Int, _ B:Int) -> Int{
    return A + B
}

let sumaRpta = suma(1, 2)

//MARK: - Clases NO es una struct

class Persona{
    //Propiedades
    var nombre:String = ""
    
    //Metodos
    func imprimirDatos(){
        print("\(nombre)")
    }
}

// STRUCT
// -->Mucho mas rapido que una clase
// -->Va por VALOR
// -->No tienen herencia, a menos que sea un PROTOCOL
struct Alumno{
    //Propiedades
    var nombre:String = ""
    var edad:Int = 0
    
    //Metodos
    func imprimirDatos(){
        print("\(nombre) \(edad)")
    }
}

var estudiante = Alumno()
estudiante.nombre = "Jhonatan"

// --> Es mas lento que un STRUCT
// --> Var por referencia
// --> Tienen herencia
class Profesor: Persona {
    //Propiedades
    var edad:Int = 0
    
    override func imprimirDatos() {
        print("\(nombre) \(edad)")
    }
}

let tutor = Profesor()
 

struct Curso{
    
    //Propiedades
    // toda las propiedades deben tener un valor por defecto
    var titulo: String
    var maestro: Profesor
    var estudiantes: [Alumno]
    
    //Constructor
    init() {
        //this == self
        self.titulo = ""
        self.maestro = Profesor()
        self.estudiantes = []
    }
    init(titulo: String, maestro: Profesor, estudiantes: [Alumno]) {
        self.titulo = titulo
        self.maestro = maestro
        self.estudiantes = estudiantes
    }
}

let iosBasico = Curso(titulo: "iOS G93", maestro: tutor, estudiantes: [estudiante])
let unCurso = Curso()

//MARK: - IF LET - SE UTILIZA PARA VALIDAR OPCIONAL
let numeroString = "32"

//Version 1
var numeroConvertido = Int(numeroString)

if numeroConvertido != nil{
    let numeroUNWRAP = numeroConvertido!
    print("El numero es: \(numeroUNWRAP)")
}

//Version 2
let numeroConvertido2 = Int(numeroString)
if let numeroUNWRAP = numeroConvertido2{
    print("El numero es: \(numeroUNWRAP)")
}

//Version 3
if let numeroUNWRAP = Int(numeroString){
    print("El numero es: \(numeroUNWRAP)")
}

// MARK: GUARD -- SE UTILIZA PARA VALIDAD OPTIONAL

func convertirAEntero(elTexto texto:String) -> Int {
    //Patron Dorado --> valida lo que no es la ruta critica, si no es de la ruta critica haz un return
    if texto.isEmpty{
        return 0
    }
    
    if texto.contains("."){
        return 0
    }
    
    guard let numero = Int(texto) else {
        return 0
    }
    
    return numero
}

